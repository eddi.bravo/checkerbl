# CheckerBl: класс для работы с сервисом http://checkerbl.pro/


## Installation

#### Composer


Из командной строки:
```
composer require bravo/checker_bl:dev-master
```

или в вашем `composer.json`
``` json
{
    "require": {
        "bravo/checker_bl": "dev-master"
    }
}

```

## Базовое использование

``` php
<?php
require 'vendor/autoload.php';

$checker = new CheckerBl\Task(':api_key');

$result = $checker->perform('ya.ru');

$result->link(); # => http://checkerbl.pro/bl_check/Dg25PX
$result->has_detects(); # => true
$result->has_detects('uribl'); # => true
$result->has_detects(['barracuda', 'sem_fresh']); # => false
$result->detects() # ['uribl']

$result->results();
/* 
Array
(
    [_sorbs_http] => false
    [_sorbs_misc] => false
    ..........
    [uribl] => true
    [spamcop] => false
)
*/

```

## Логирование
Вы можете передать в опции **ваш** класс логирования, который реализует интерфейс [Psr\Log\LoggerInterface](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md#3-psrlogloggerinterface)

``` php
<?php 

$logger = new YourLogger('./logs', YourLogger::DEBUG);
$checker = new CheckerBl\Task(':api_key', array(
    'logger' => $logger
));
```

## Отложенная проверка
``` php
<?php
# ........
$response = $checker->perform('ya.ru', 'later');

//... do stuff

do{
    $response = $checker->get($response->id);
    sleep(1);
}while($response->response_code()==206);


```

## Ошибки
При получения статуса отличного от `2**` выбрасываются исключения

| Имя класса                       	| Описанине                                                                                            	|
|----------------------------------	|---------------------------------------------------------------------------------------------------	|
| `CheckerBl\ThreadsLimitException`  	| При получении статуса **429**                                                                       	|
| `CheckerBl\UnProcessableException` 	| При получении статуса **422**. Доступен метод `getErrors()` который содержит массив ошибок.         	|
| `CheckerBl\TimeoutException`       	| При получении статуса **408**. Доступен метод `getTaskId()`, который вернет `id` созданной задачи.  	|
| `CheckerBl\UnAuthorizedException`   | При получении статуса **401**.  	                                                                    |
| `CheckerBl\ResponseError`           | При получении статуса отличного от **2\*\*** и выше перечисленных.                                    |


## Что ещё есть внутри?

Если взглянуть на `API` сервиса, то мы увидим примерно следующее:

```
{
  "bl_checker_task": {
    "id": "pxjDgB",
    "hostname": "example.com",
    "resolved_ip": "93.184.216.34",
    "results": [
      {
        "sys_name": "_sorbs_http",
        "human_name": "SORBS HTTP",
        "status": "clear"
      },
      {
        "sys_name": "_sorbs_socks",
        "human_name": "SORBS SOCKS",
        "status": "dirty"
      },
      {
        "sys_name": "sorbs_spam",
        "human_name": "SORBS Spam",
        "status": "check_error"
      }
    ]
  }
}
```

К каждому ключу хеша `bl_checker_task` вы можете обращаться напрямую вот так:

``` php
<?php 
# ....init 
$response = $checker->perform('ya.ru');
$response->hostname; # => ya.ru
$reponse['hostname']; # => ya.ru
$response->id; # pxjDgB
$responee['id']; # pxjDgB
.......

```