<?php

namespace CheckerBl;
class Task
{
    const SERVER = 'checkerbl.pro';
    const API_VERSION = 1;

    protected $api_key;
    protected $options;
    protected $logger;

    public function __construct($api_key, $options=[])
    {
        $this->api_key = $api_key;

        $default_options=array(
            'server' => Task::SERVER,
            'api_version' => Task::API_VERSION,
            'timeout' => 60,
            'logger' => null
        );

        $_options=array_merge($default_options, $options);

        $this->logger = new Logger($_options['logger']);
        unset($_options['logger']);

        $this->options = $_options;

        $this->logger->debug("Initialize checker with following data:", $this->options);
    }

    /**
     * @param $hostname <p>checking target
     * @param string $response_type <p>now | later
     * @param array $bl_checkers<p>Array blacklist services to check
     * @return Response
     */
    public function perform($hostname, $response_type='now', $bl_checkers=[])
    {
        $response = $this->request(array(
            'method' => 'POST',
            'content' => json_encode(array(
                'hostname' => $hostname,
                'response_type' => $response_type,
                'checker_list' => $bl_checkers
            ))
        ), $this->baseurl());

        return $response;
    }

    public function get($id){
        $response = $this->request(array(
            'method' => 'GET',
        ), $this->baseurl() . "/{$id}");

        return $response;
    }

    protected function request($options, $url)
    {
        $this->logger->info("Request to {$url} with the following options: ", $options);
        $headers = array(
            "X-Api-Key: {$this->api_key}",
            'Content-Type: application/json'
        );
        $default_options =array(
            'method'  => 'GET',
            'ignore_errors' => true,
            'header'  => implode($headers, "\r\n")."\r\n",
            'timeout' => $this->options['timeout']
        );
        $opts = array('http' => array_merge($default_options, $options));
        $body = file_get_contents($url, false, stream_context_create($opts));

        return new Response($http_response_header, json_decode($body, true), $this->options['server'], $this->logger);
    }

    protected function baseurl()
    {
        return "https://{$this->options['server']}/api/v{$this->options['api_version']}/bl_check";
    }
}