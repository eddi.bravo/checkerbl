<?php

namespace CheckerBl;

class Response implements \ArrayAccess
{
    protected $headers;
    protected $api_result;
    private $logger;
    private $server;
    private $_json_answer;

    public function __construct($http_response_header, $json, $server, $logger)
    {
        $this->headers = $this->_parse_headers($http_response_header);
        $this->_json_answer= $json;
        $this->api_result= isset($this->_json_answer['bl_checker_task']) ? $this->_json_answer['bl_checker_task'] : array();
        $this->server = $server;
        $this->logger = $logger;

        $this->logger->info("Response status: {$this->response_code()}");
        $this->logger->debug("Response body: ", $this->_json_answer);

        $this->check_response_status();
    }

    /**
     * Return response code
     * @return integer
     */
    public function response_code()
    {
        return $this->headers['response_code'];
    }

    /**
     * Return link to check result
     * @return string
     */

    public function link()
    {
        return "https://{$this->server}/bl_check/{$this['id']}";
    }

    /**
     * @param string | array | null $blacklist
     *
     * @return boolean
     */
    public function has_detects($blacklist = null){
        if(is_null($blacklist)){
            return !empty(array_search(true, $this->results()));
        }elseif(is_array($blacklist)){
            $bool = false;
            foreach ($blacklist as $bl_name){
                if($this->_has_detects_for($bl_name)){
                    $bool = true;
                    break;
                }
            }
            return $bool;
        }else{
            return $this->_has_detects_for($blacklist);
        }
    }

    /**
     * Return array of detects
     * @param string $key
     * @return array
     */
    public function detects($key='sys_name'){
        return array_keys(array_filter($this->results($key), function($v){
            return $v;
        }));
    }

    /**
     * Check result
     * @param string $key <p>
     * Must be sys_name or human_name
     * @return array
     * Return array of results. <p>
     * Keys for returned array are blacklist service names, value mixed:<p>
     * - true if detected.<p>
     * - false if clean.<p>
     * - null if error
     * @throws \InvalidArgumentException <p>
     * if key param not included in list - sys_name, human_name
     * </p>
     */
    public function results($key='sys_name')
    {
        $return = array();

        if(!in_array($key, array('sys_name', 'human_name'))){
            throw new \InvalidArgumentException('Wrong key. Only `sys_name` and `human_name` are supported');
        }

        foreach($this->api_result['results'] as $result){
            $return[$result[$key]] = $result['status']=='clear' ? false : ($result['status']=='check_error' ? null : true);
        }
        return $return;
    }

    /**
     * Return array of original response in JSON
     * @return array
     */
    public function getApiResult()
    {
        return $this->api_result;
    }

    /**
     * Return array of response headers
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    private function check_response_status(){
        $code = (string)$this->response_code();
        if($code[0] != '2'){
            try{
                switch ($this->response_code()){
                    case 401:
                        throw new UnAuthorizedException('Wrong api key');
                        break;
                    case 408:
                        $error = new TimeoutException('Timeout error');
                        $error->setTaskId($this['id']);
                        throw $error;
                        break;
                    case 422:
                        $error = new UnProcessableException('Un processable entity');
                        $error->setErrors($this->_json_answer['errors']);
                        throw $error;
                        break;
                    case 429:
                        throw new ThreadsLimitException('Too many threads');
                    default:
                        throw new ResponseError("Wrong response code {$this->response_code()}");
                }
            }catch (\Exception $e){
                $this->logger->error($e->getMessage());
                throw $e;
            }

        }
    }

    private function _has_detects_for($blacklist){
        return $this->results()[$blacklist] || false;
    }
    private function _parse_headers($headers )
    {
        $head = array();
        foreach( $headers as $k=>$v )
        {
            $t = explode( ':', $v, 2 );
            if( isset( $t[1] ) )
                $head[ trim($t[0]) ] = trim( $t[1] );
            else
            {
                $head[] = $v;
                if( preg_match( '#HTTP/[0-9\.]+\s+([0-9]+)#',$v, $out ) )
                    $head['response_code'] = intval($out[1]);
            }
        }
        return $head;
    }

    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    public function offsetExists($offset)
    {
        return isset($this->api_result[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->api_result[$offset];
    }

    public function offsetSet($offset, $value)
    {
        // cant set
    }

    public function offsetUnset($offset)
    {
        //cant unset
    }

    /**
     * @return mixed
     */
    public function getJsonAnswer()
    {
        return $this->_json_answer;
    }
}
