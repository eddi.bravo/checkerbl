<?php

namespace CheckerBl;

class TimeoutException extends \Exception
{
    protected $task_id;

    /**
     * @return mixed
     */
    public function getTaskId()
    {
        return $this->task_id;
    }

    /**
     * @param mixed $task_id
     */
    public function setTaskId($task_id)
    {
        $this->task_id = $task_id;
    }
}