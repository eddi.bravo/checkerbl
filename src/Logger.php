<?php

namespace CheckerBl;

class Logger
{
    protected $logger;
    public function __construct($logger=null)
    {
        $this->logger = $logger;
    }

    public function __call($level, $arguments)
    {
        $msg = $arguments[0];
        $context = isset($arguments[1]) ? $arguments[1] : array();
        if($this->logger){
            $this->logger->$level($msg, $context);
        }
    }
}